package fr.univlille.iut.r304.ctp;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.beans.Transient;
import java.util.HashMap;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import fr.univlille.iut.r304.cpt.View.Model.Personne;
import fr.univlille.iut.r304.cpt.View.Model.Plateforme;

public class PlateformeTest {
    private Plateforme plateforme;

    @BeforeEach
    public void setup(){
        plateforme = new Plateforme();
    }

    @Test
    public void testCreateEmptyPlatforme(){
        assertTrue(plateforme.isEmpty());
    }

    @Test
    public void testAddPersonneInPlatforme(){
        Personne personne = new Personne("Didier");
        assertTrue(plateforme.add(personne));
        assertEquals(plateforme.get(0),personne);
    }

    @Test
    public void testEchangeCadeau(){
        Personne personne1 = new Personne("Didier");
        Personne personne2 = new Personne("Bee");
        plateforme.add(personne1);
        plateforme.add(personne2);
        HashMap<Personne,Personne> listeEchange = plateforme.echangeCadeau();
        assertEquals(listeEchange.get(personne2),personne1);
        assertEquals(listeEchange.get(personne1),personne2);
    }
}
