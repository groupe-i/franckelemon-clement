package fr.univlille.iut.r304.ctp;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.univlille.iut.r304.cpt.View.Model.Personne;

public class PersonneTest {
    private Personne personne;

    @Test
    public void testCreatePersonne(){
        personne = new Personne("Didier");
        assertEquals(personne.getName(),"Didier");
    }

}
