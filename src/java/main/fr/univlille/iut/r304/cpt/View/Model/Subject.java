package fr.univlille.iut.r304.cpt.View.Model;

import java.util.ArrayList;

public abstract class Subject {
    ArrayList<Observer> observers;

    public void attach(Observer observer){
        observers.add(observer);
    }
    public void detach(Observer observer){
        observers.remove(observer);
    }
    protected void notifyObservers(){
        for(Observer observer : observers){
            observer.update(this);
        }
    }
    protected void notifyObservers(Object obj){
        for(Observer observer : observers){
            observer.update(this,obj);
        }
    }
}
