package fr.univlille.iut.r304.cpt.View.View;

import java.util.HashMap;
import java.util.Map.Entry;

import fr.univlille.iut.r304.cpt.View.Controller.Controller;
import fr.univlille.iut.r304.cpt.View.Model.Observer;
import fr.univlille.iut.r304.cpt.View.Model.Personne;
import fr.univlille.iut.r304.cpt.View.Model.Subject;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class SampleView extends Application implements Observer {
    public static void main(String[] args) {
        launch(args);
    }

    Controller ctrl;

    TextArea circularContent;

    TextArea alphaContent;

    TextArea listPersonne;

    @Override
    public void start(Stage primaryStage) {

        ctrl = new Controller();
        ctrl.init();

        VBox left = inscriptionInit();

        VBox right = echangeInit();

        HBox root = new HBox(left, right);
        Scene scene = new Scene(root, 400, 400);
        primaryStage.setTitle("Tirage au sort des cadeaux");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private VBox inscriptionInit(){
        VBox left = new VBox();
        Button addBtn = new Button();
        Label title = new Label("Inscrits :");
        TextField input = new TextField();
        input.setMinSize(10, 30);
        addBtn.setText("+");
        ctrl.buttonAdd(addBtn,input);
        listPersonne = new TextArea();
        left.getChildren().addAll(title, input, addBtn,listPersonne);
        return left;
    }

    private VBox echangeInit(){
        VBox right = new VBox();
        Button btn = new Button();
        btn.setText("Nouveau tirage !");
        ctrl.buttonTirage(btn);
        Label circularTitle = new Label("Affichage circulaire");
        circularContent = new TextArea();
        circularContent.appendText("Example1");
        circularContent.appendText("\n -> Example2");
        circularContent.setPrefSize(250,100);
        Label alphaTitle = new Label("Affichage alphabétique");
        alphaContent = new TextArea();
        alphaContent.setPrefSize(250,100);
        alphaContent.appendText("Example1 -> Example2");
        right.getChildren().addAll(btn, circularTitle, circularContent, alphaTitle, alphaContent);
        return right;
    }

    private void echangeUpdate(HashMap<Personne,Personne> newList){
        circularContent.clear();
        boolean first = true;
        for(Entry<Personne, Personne> echange : newList.entrySet()){
            if (first){
                first = false;
                circularContent.appendText(echange.getKey().getName());
                circularContent.appendText("\n -> "+echange.getValue().getName());
            } else {
                circularContent.appendText("\n -> "+echange.getKey().getName());
                circularContent.appendText("\n -> "+echange.getValue().getName());
            }
        }
        alphaContent.clear();
        for(Entry<Personne, Personne> echange : newList.entrySet()){
            alphaContent.appendText(echange.getKey().getName()+" -> "+echange.getValue().getName()+"\n");
        }
    }

    @Override
    public void update(Subject subject) {
        // tmp
    }

    @Override
    public void update(Subject subject, Object obj) {
        if(obj instanceof Personne){
            Personne personne = (Personne) obj;
            listPersonne.appendText(personne.getName());
        } else {
            HashMap<Personne,Personne> listEchange = (HashMap<Personne,Personne>) obj;
            echangeUpdate(listEchange);
        }
    }
}

