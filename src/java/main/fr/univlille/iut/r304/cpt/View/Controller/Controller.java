package fr.univlille.iut.r304.cpt.View.Controller;

import fr.univlille.iut.r304.cpt.View.Model.Personne;
import fr.univlille.iut.r304.cpt.View.Model.Plateforme;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {

    Plateforme plateforme;
    
    public void init(){
        plateforme = new Plateforme();
    }

    public void buttonAdd(Button button,TextField name){
        button.setOnAction(e->{
            plateforme.add(new Personne(name.getText()));
        });
    }

    public void buttonTirage(Button button){
        button.setOnAction(e->{
            plateforme.echangeCadeau();
        });
    }
}
