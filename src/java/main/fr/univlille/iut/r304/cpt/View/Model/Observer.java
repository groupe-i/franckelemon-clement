package fr.univlille.iut.r304.cpt.View.Model;

public interface Observer {
    public void update(Subject subject);
    public void update(Subject subject, Object obj);
}
