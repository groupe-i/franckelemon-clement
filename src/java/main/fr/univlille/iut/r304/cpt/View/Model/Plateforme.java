package fr.univlille.iut.r304.cpt.View.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Plateforme extends Subject {
    private ArrayList<Personne> inscrits;

    public Plateforme(){
        inscrits = new ArrayList<>();
    }

    public boolean isEmpty(){
        return inscrits.isEmpty();
    }

    public boolean add(Personne personne){
        return inscrits.add(personne);
    }

    public Personne get(int idx){
        return inscrits.get(idx);
    }

    public HashMap<Personne,Personne> echangeCadeau(){
        HashMap<Personne,Personne> listEchange = new HashMap<>();
        melangeListInscrit();
        for(int i=0; i<inscrits.size();i++){
            listEchange.put(inscrits.get(i), inscrits.get((i+1)%inscrits.size()));
        }
        notifyObservers(listEchange);
        return listEchange;
    }

    private void melangeListInscrit(){
        ArrayList<Personne> copy = new ArrayList<>();
        Random r = new Random();
        for (Personne p : inscrits){
            copy.add(p);
        }
        for(int i=0; i<inscrits.size();i++){
            inscrits.set(i, copy.get(r.nextInt(copy.size())));
        }
    }
}

